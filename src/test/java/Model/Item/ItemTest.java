package Model.Item;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ItemTest {

    iItem item1;
    iItem item2;
    String url1 = "E:\\work\\parfume\\src\\test\\resources\\page.html";
    String url2 = "E:\\work\\parfume\\src\\test\\resources\\page2.html";

    @Before
    public void initialize(){
        String page = "";
        try {
            Scanner in = new Scanner(new File(url1));
            while(in.hasNext())
                page += in.nextLine() + "\r\n";
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        item1 = new Item(page);
        item1.parse();

        page = "";
        try {
            Scanner in = new Scanner(new File(url2));
            while(in.hasNext())
                page += in.nextLine() + "\r\n";
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        item2 = new Item(page);
        item2.parse();
    }

    @Test
    public void testGetRegularPrice() throws Exception {
        assertEquals("2460", item1.getRegularPrice());
        assertEquals("2400", item2.getRegularPrice());
    }

    @Test
    public void testGetTotalPrice() throws Exception {
        assertEquals("1165", item1.getTotalPrice());
        assertEquals("1236", item2.getTotalPrice());
    }

    @Test
    public void testGetName() throws Exception {
        assertEquals("Giorgio Armani \"Acqua di Gioia\" edT 50ml", item1.getName());
        assertEquals("Antonio Banderas \"Blue Seduction Splash for Women\"", item2.getName());
    }

    @Test
    public void testGetDescription() throws Exception {
        assertEquals("Женский", item1.getDescription());
        String page2Desctiption = "Женский цветочный фруктовый парфюм Splash Blue Seduction for Women создан американским одноименным модным брендом знаменитости Antonio Banderas в 2012 году. Этот беззаботный, бодрящий, легкий, сексуальный, в меру сладкий аромат от Antonio Banderas является частью линии “ Splash Seduction ” данного дома и выпущен как освежающий цветочно-фруктовый парфюм. Духи раскрываются сочными, спелыми фруктами и переходят в красивый, многогранный цветочный аромат с утонченными лесными нотами. В букет композиции Splash Blue Seduction for Women входит груша, персик, черная смородина, тангерин, гардения, жасмин, малина, болгарская роза, фрезия, древесные ароматы и мускус. Парфюм отличается превосходной стойкостью, подходит для использования в качестве дневных духов, лучше всего раскрывается летом. " +
                "Верхние ноты: груша, персик, черная смородина, тангерин; Ноты сердца: гардения, жасмин, малина, болгарская роза, фрезия; Базовые ноты: древесные ноты, мускус. " +
                "Семейства парфюма: фруктовые, цветочные.";
        assertEquals(page2Desctiption, item2.getDescription());
    }

    @Test
    public void testGetManufacture() throws Exception {

    }


    @Test
    @Ignore
    public void testGetImage() throws Exception {
    //    assertEquals("Giorgio-Armani-Acqua-di-Gioia_enl.jpg", item1.getImage());
      //  assertEquals("http://parfumstok.ru/published/publicdata/ASISTPARFUM/attachments/SC/products_pictures/Giorgio-Armani-Acqua-di-Gioia_enl.jpg", item2.);

     //   assertEquals("antonio-banderas-splash-blue-seduction-for-woman_enl.jpg", item2.getImage());
       // assertEquals("http://parfumstok.ru/published/publicdata/ASISTPARFUM/attachments/SC/products_pictures/antonio-banderas-splash-blue-seduction-for-woman_enl.jpg", item2.getImage());
    }


    @Test
    public void testEquals() throws Exception {
        assertFalse("not equals",item1.equals(item2));
        assertTrue("equals", item1.equals(item1));
    }
}