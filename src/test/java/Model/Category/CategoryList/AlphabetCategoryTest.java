package Model.Category.CategoryList;

import org.jsoup.Jsoup;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class AlphabetCategoryTest {
    @Test
    public void testGetItems() throws Exception {

    }

    @Test
    public void testGetCountPage() throws Exception {
        List<String> countPage = new ArrayList<String>();
        countPage.add("E:\\work\\parfume\\src\\test\\resources\\countPage1.html");
        countPage.add("E:\\work\\parfume\\src\\test\\resources\\countPage2.html");
        countPage.add("E:\\work\\parfume\\src\\test\\resources\\countPage3.html");
        countPage.add("E:\\work\\parfume\\src\\test\\resources\\countPage4.html");

        AlphabetCategory alphabetCategory = new AlphabetCategory();
        List<String> page = new ArrayList<String>();
        for (String s : countPage) {
           page.add(getPage(s));
        }

        org.jsoup.nodes.Document doc = Jsoup.parse(page.get(0));
        assertEquals(2, alphabetCategory.getCountPage(doc));

         doc = Jsoup.parse(page.get(1));
        assertEquals(2, alphabetCategory.getCountPage(doc));

        doc = Jsoup.parse(page.get(2));
        assertEquals(1, alphabetCategory.getCountPage(doc));

         doc = Jsoup.parse(page.get(3));
        assertEquals(3, alphabetCategory.getCountPage(doc));



    }

    private String getPage(String url){
        String page = "";
        try {
            Scanner in = new Scanner(new File(url));
            while(in.hasNext())
                page += in.nextLine() + "\r\n";
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return page;
    }

}