package Model.Category;

import Model.Category.CategoryList.iCategoryList;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class AllCategoryTest {

    private AllCategory allCategory;

    @Before
    public void initialize(){
        String page = "";
        try {
            String url = "E:\\work\\parfume\\src\\test\\resources\\page.html";
            Scanner in = new Scanner(new File(url));
            while(in.hasNext())
                page += in.nextLine() + "\r\n";
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
         allCategory = new AllCategory(page);
        allCategory.parse();
    }



    @Test
    public void testGetPerfumeCategories() throws Exception {

        iCategoryList categories = allCategory.getPerfumeCategories();

        iCategory category = categories.get(0);

        assertEquals("Туалетная вода",category.getName());
        assertEquals("http://parfumstok.ru/category/parfum/", category.getUrl());

        category = categories.get(categories.size() - 1);

        assertEquals("Тестеры",category.getName());
        assertEquals("http://parfumstok.ru/search/?searchstring=%D1%82%D0%B5%D1%81%D1%82%D0%B5%D1%80", category.getUrl());

    }

    @Test
    public void testGetMaleCategories() throws Exception {

        iCategoryList categories = allCategory.getMaleCategories();

        iCategory category = categories.get(0);

        assertEquals("Для мужчин",category.getName());
        assertEquals("http://www.parfumstok.ru/search/?searchstring=%D0%BC%D1%83%D0%B6%D1%81%D0%BA%D0%BE%D0%B9", category.getUrl());

        category = categories.get(categories.size() - 1);

        assertEquals("Для женщин",category.getName());
        assertEquals("http://www.parfumstok.ru/search/?searchstring=%D0%B6%D0%B5%D0%BD%D1%81%D0%BA%D0%B8%D0%B9", category.getUrl());


    }

    @Test
    public void testGetAlphabetCategories() throws Exception {

        iCategoryList categories = allCategory.getAlphabetCategories();

        iCategory category = categories.get(0);

        assertEquals("Adidas",category.getName());
        assertEquals("http://parfumstok.ru/category/adidas/", category.getUrl());

        category = categories.get(categories.size() - 1);

        assertEquals("Yves Saint Laurent",category.getName());
        assertEquals("http://parfumstok.ru/category/yves-saint-laurent/", category.getUrl());
    }
}