import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Cruze on 14.02.2015.
 */
public class FromExcelToCsv {

    static String url = "E:\\work\\parfume\\pair.xls";

    public static void main(String[] args) throws FileNotFoundException {

        ArrayList<String> csv = getCsv();
        String perfume = "";
        for (String s : csv) {
            perfume = perfume + s + "\n";
        }


        Map<String, String> pair = new TreeMap<String, String>();


        pair = getPairFromExcel("pair.xls");

        for (Map.Entry<String, String> entry : pair.entrySet()) {
            if (perfume.contains(entry.getKey())){
                perfume = perfume.replaceAll(entry.getKey(), entry.getValue());
                System.out.println("was replace: " + entry.getKey() + " = " + entry.getValue());
            }
        }

        File file = new File("lastVersion.csv");
        PrintWriter out = new PrintWriter(file);
        out.print(perfume);
        out.close();

    }

    private static Map<String, String> getPairFromExcel(String url) {
        Map<String, String> pair = new TreeMap<String, String>();
        HSSFWorkbook wb = getHSSFWorkbook();
        Sheet sheet = wb.getSheetAt(0);
        for (Row row : sheet) {
            if (row.getCell(0) != null) {
                String old = row.getCell(0).getStringCellValue();
                String yang = row.getCell(1).getStringCellValue();
                pair.put(old, yang);
            }
        }
        return pair;
    }


    private static HSSFWorkbook getHSSFWorkbook() {
        try {
            return new HSSFWorkbook(getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("i can't initialize new HSSFWorkbook");
            return null;
        }
    }

    private static InputStream getInputStream() {
        try {
            return new FileInputStream(url);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("I can't find this file: " + url);
            return null;
        }
    }


    private static ArrayList<String> getCsv() {
        File file = new File("newPerfume.csv");

        BufferedReader br = null;

        br = getBufferedReader(file);

        ArrayList<String> strings = new ArrayList<String>();

        String line = null;

        try {
            while ((line = br.readLine()) != null) {
                strings.add(line);
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }


        close(br);
        return strings;
    }

    private static void close(BufferedReader br)  {
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static BufferedReader getBufferedReader(File file)  {
        BufferedReader br = null;
        try {
            br = new BufferedReader(
                    new InputStreamReader( new FileInputStream( file ), "UTF-8")
            );
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return br;
    }

}
