import Model.CSV.CSVFile;
import Model.Category.AllCategory;
import Model.Category.Category;
import Model.Category.CategoryList.iCategoryList;
import Model.Item.ItemList;
import Model.Item.iItem;

import static Model.Utils.Utils.PARFUMESTOK;
import static Model.Utils.Utils.getPage;

/**
 * Created by Cruze on 01.02.2015.
 * main class
 */
public class Main {

    public static void main(String[] args) {

        String page = getPage(PARFUMESTOK);

        AllCategory allCategory = new AllCategory(page);

        ItemList items = new ItemList();

        for (iCategoryList categoryList : allCategory.getCategories()) {
            items.add(categoryList.getItems());
            System.out.println("in main");
        }
        System.out.println("size: " + items.getItems().size());

        for (iItem iItem : items.getItems()) {
            if (iItem.getDescription().equals("")){
                iItem.parse();
            }
        }

        CSVFile csv = new CSVFile("perfume.csv");
        csv.add(items);
        csv.write();
    }
}

