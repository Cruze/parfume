package Excel;

import Model.CSV.CSVFile;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Cruze on 12.02.2015.
 * this class add information from excel to csv
 */
public class ExcelAndCsv {

    public static void main(String[] args) throws FileNotFoundException {

        CSVFile csv = new CSVFile("E:\\work\\parfume\\perfume.csv");
        ItemNames csvNames = csv.getNames();

        Excel excel = new Excel("E:\\work\\parfume\\Nalichie_Dukhi.xls");
        ItemNames excelNames = excel.getNames();


        ItemNames csvNamesForFound = getCSVNamesForFound(csvNames);

        ItemNames excelNamesForFound = getExcelNamesForFound(excelNames);


        Map<Integer, Integer> pair = getIndexPair(csvNamesForFound, excelNamesForFound);



        List<Integer> allIndex = new ArrayList<Integer>();
        List<Integer> indexNotFound = new ArrayList<Integer>();

        for (int i = 0; i < csvNames.getNames().size(); i++) {
            allIndex.add(i);
        }



        for (Integer integer : allIndex) {
            if (!pair.containsKey(integer)){
                indexNotFound.add(integer);
            }
        }


        HSSFWorkbook wb = new HSSFWorkbook(); // keep 100 rows in memory, exceeding rows will be flushed to disk
        Sheet sh = wb.createSheet();
        int rowCount = 0;


        for (Integer integer : indexNotFound) {
            String name = csvNames.getNames().get(integer);
         //   System.out.println(rowCount + ": " + name + " = " + csvNamesForFound.getNames().get(integer));
            Row row = sh.createRow(rowCount++);
            Cell cellCsv = row.createCell(0);
            cellCsv.setCellValue(name);
            try {

                File fileExcel = new File("notFound.xls");
                FileOutputStream outer =
                        new FileOutputStream(fileExcel);
                wb.write(outer);
                outer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        ItemNames newNames = new ItemNames();


         wb = new HSSFWorkbook(); // keep 100 rows in memory, exceeding rows will be flushed to disk
         sh = wb.createSheet();
         rowCount = 0;



        for (Map.Entry<Integer, Integer> entry : pair.entrySet())
        {
            String csvName = csvNames.getNames().get(entry.getKey());
            String excelName = excelNames.getNames().get(entry.getValue());
            String name = buildNewName(csvName, excelName);
            newNames.add(name);

            Row row = sh.createRow(rowCount++);
            Cell cellCsv = row.createCell(0);
            Cell cellExcel = row.createCell(1);
            Cell cellResult = row.createCell(2);
            cellCsv.setCellValue(csvName);
            cellExcel.setCellValue(excelName);
            cellResult.setCellValue(name);
            try {

                File fileExcel = new File("log.xls");
                FileOutputStream outer =
                        new FileOutputStream(fileExcel);
                wb.write(outer);
                outer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }




    }


    private static Map<Integer, Integer> getIndexPair(ItemNames csvNamesForFound, ItemNames excelNamesForFound) {
        Map<Integer, Integer> pair = new LinkedHashMap<Integer, Integer>();

        for (String csvName : csvNamesForFound.getNames()) {

            if (excelNamesForFound.isContain(csvName)){
                int indexCSV = csvNamesForFound.getNames().indexOf(csvName);
                int indexExcel = excelNamesForFound.getIndex(csvName);
                pair.put(indexCSV, indexExcel);
            }
        }
        return pair;
    }

    private static ItemNames getExcelNamesForFound(ItemNames excelNames) {
        ItemNames excelNamesForFound = new ItemNames();
        for (String s : excelNames.getNames()) {
            String name = getFoundNameExcel(s);
            excelNamesForFound.add(name);
        }
        return excelNamesForFound;
    }

    private static ItemNames getCSVNamesForFound(ItemNames csvNames) {
        ItemNames csv = new ItemNames();
        for (String s : csvNames.getNames()) {
            String name = getFoundNameCSV(s);
            csv.add(name);
        }
        return csv;
    }

    private static String getFoundNameExcel(String s) {
        String name = s.replaceAll(" ", "");


        name = name.replaceAll("edT", "");
        name = name.replaceAll("edP", "");
        name = name.replaceAll("[0-9]", "");
        name = name.replaceAll("ml", "");

        name = name.replaceAll("\"", "").toLowerCase();
        return name;
    }

    private static String getFoundNameCSV(String s) {
        String name = s;


        name = name.replaceAll("Antonio Banderas", "A.Banderas");
        name = name.replaceAll("Agua", "Aqua");
        name = name.replaceAll("от Agent Provocateur", "");
        name = name.replaceAll("Agent Provocateur", "");
        name = name.replaceAll("Agatha Ruiz de La Prada \"Corazon\"", "Agatha Cosazon");
        name = name.replaceAll("Aramis Life от Aramis", "Aramis Life");
        name = name.replaceAll("Calvin Klein", "C.Klein");
        name = name.replaceAll("Christian Dior \"Chris", "Christian Dior \"Cris");
        name = name.replaceAll("Christian Dior", "C.Dior");
        name = name.replaceAll("Dolce And Gabbana", "D&G");
        name = name.replaceAll("Yves Saint Laurent", "YSL");
        name = name.replaceAll("Giorgio Armani", "G.Armani");
        name = name.replaceAll("Salvatore Ferragamo", "S.Ferragamo");
        name = name.replaceAll("Nina Ricci", "N.Ricci");
        name = name.replaceAll("Jean Paul Gaultier", "J.P.Gaultier");
        name = name.replaceAll("Tommy Hilfiger", "T.Hilfiger");


        name = name.replaceAll(" ", "");
        name = name.replaceAll("\"", "");
        name = name.replaceAll("edT", "");
        name = name.replaceAll("edP", "");
        name = name.replaceAll("[0-9]", "");
         name = name.replaceAll("ml", "");
        name = name.replaceAll("[а-яА-Я]+", "");
        name = name.replaceAll("\\+", "");



        name = name.replaceAll("for Men", "");
        name = name.replaceAll("for Women", "");
        name = name.replaceAll("for Men", "");
        name = name.replaceAll("For Women", "");
        name = name.replaceAll("Man", "");
        name = name.replaceAll("woman", "");
        name = name.replaceAll("original", "");
        name = name.replaceAll("tester", "");

        name = name.toLowerCase();




        if (name.length()!= 0)
        if (name.substring(name.length()-3, name.length()).equals("men"))
            name = name.substring(0, name.length()-3);

        if (name.length()!= 0)
            if (name.substring(name.length()-3, name.length()).equals("for"))
                name = name.substring(0, name.length()-3);


        return name;
    }

    private static String buildNewName(String nameCsv, String nameExcel) {
        //Gucci "Guilty Glossy"woman edT 75ml = Gucci"Guilty Glossy"" woman edT 75ml NEW
        nameCsv = replace(nameCsv);
        if (!nameCsv.contains(getType(nameExcel))){
        nameCsv = nameCsv + " " + getType(nameExcel);
        }
        nameCsv = nameCsv + " " + getMl(nameExcel);
        nameCsv = nameCsv + getTester(nameExcel);

        return nameCsv;
    }

    private static String getTester(String nameExcel) {
        if (nameExcel.toLowerCase().contains("тестер")){
            return ", тестер";
        }
        return "";
    }

    private static String getMl(String nameExcel) {
        Pattern pattern = Pattern.compile("(\\d+\\,\\d+)ml");
        Matcher matcher = pattern.matcher(nameExcel);
        if (matcher.find()) {
            return matcher.group(1) + " мл";
        } else {
            pattern = Pattern.compile("(\\d+\\.\\d+)ml");
            matcher = pattern.matcher(nameExcel);
            if (matcher.find()) {
                return matcher.group(1) + " мл";
            } else {
                pattern = Pattern.compile("(\\d+)ml");
                matcher = pattern.matcher(nameExcel);
                if (matcher.find()) {
                    return matcher.group(1) + " мл";
                } else {
                    pattern = Pattern.compile("(\\d+) мл");
                    matcher = pattern.matcher(nameExcel);
                    if (matcher.find()) {
                        return matcher.group(1) + " мл";
                    } else {
                        pattern = Pattern.compile("(\\d+) ml");
                        matcher = pattern.matcher(nameExcel);
                        if (matcher.find()) {
                            return matcher.group(1) + " мл";
                        }
                        pattern = Pattern.compile("(\\d+) ml");
                        matcher = pattern.matcher(nameExcel);
                        if (matcher.find()) {
                            return matcher.group(1) + " мл";
                        } else {
                            pattern = Pattern.compile("(\\d+)g");
                            matcher = pattern.matcher(nameExcel);
                            if (matcher.find()) {
                                return matcher.group(1) + " гр";
                            }
                        }
                    }
                }
            }
        }
        return "";
    }

    private static String getType(String nameExcel) {
        if (nameExcel.contains("edT")){
            return "туалетная вода";
        } else if (nameExcel.contains("edP")){
         return "парфюмерная вода";
        } else if (nameExcel.toLowerCase().contains("сухие духи")){
            return "сухие духи";
        } else if (nameExcel.toLowerCase().contains("духи")){
            return "духи";
        } else {
            return "";
        }
    }

    private static String replace(String nameCsv) {
        nameCsv =  nameCsv.replaceAll("edP", "");
        nameCsv = nameCsv.replaceAll("edT", "");
        nameCsv = nameCsv.replaceAll("(\\d+)ml", "").trim();
        //TODO replace ml number
        return nameCsv;
    }
}
