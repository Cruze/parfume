package Excel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Cruze on 12.02.2015.
 * class for work with excel
 */
public class Excel {

    String url;
    ItemNames names = new ItemNames();


    public ItemNames getNames() {
        return names;
    }

    public Excel(String url) {
        this.url = url;
        getItemNames();
    }

    private void getItemNames() {

        HSSFWorkbook wb = getHSSFWorkbook();
        Sheet sheet = wb.getSheetAt(0);
        for (Row row : sheet) {
            for (Cell cell : row) {
                int cellType = cell.getCellType();
                switch (cellType) {
                    case Cell.CELL_TYPE_STRING:
                        names.add(cell.getStringCellValue());
                        break;
                    default:
                        break;
                }
            }
        }

    }


    private HSSFWorkbook getHSSFWorkbook() {
        try {
            return new HSSFWorkbook(getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("i can't initialize new HSSFWorkbook");
            return null;
        }
    }

    private InputStream getInputStream() {
        try {
            return new FileInputStream(url);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("I can't find this file: " + url);
            return null;
        }
    }

    public void add(String csvName, String excelName, String name) {

    }
}
