package Excel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 1 on 03.01.2015.
 * collection of item names
 */
public class ItemNames {

    List<String> names = new ArrayList<String>();

    public void add(String name){
        names.add(name);
    }

    public List<String> getNames() {
        return names;
    }

    public int size() {
        return names.size();
    }

    public boolean isContain(String name) {

        for (String s : names) {
            if (s.contains(name)) { //TODO this fucntion should be
                return true;
            }
        }
       return false;
    }


    public int getIndex(String csvName) {
        for (String name : names) {
            if (name.contains(csvName)) {
                return names.indexOf(name);
            }
        }
        return 0;
    }
}
