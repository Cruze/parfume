package Model.Item;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import static Model.Utils.Utils.getPage;

/**
 * Created by Cruze on 01.02.2015.
 * 1 item of parfume
 */
public class Item implements iItem {

    private String page = "";
    private String url = "";

    List<String> categories = new ArrayList<String>();

    private String regularPrice;
    private String totalPrice;

    private String name;
    private String description = "";
    private Image  image;


    public void addCategory(String category){
        if (!categories.contains(category)){
            categories.add(category);
        }
    }

    @Override
    public List<String> getCategory() {
        return categories;
    }


    @Override
    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }

    private String manufacture;

    public Item(String page) {
        this.page = page;
    }

    @Override
    public void parse(){

        if (page.equals("")){
            page = getPage(url);
            System.out.println("this situation is fine");
        }

        org.jsoup.nodes.Document doc = Jsoup.parse(page);
        Elements elements = doc.select("div[class=main-content]");
        name = elements.select("div[class=cpt_product_name]").text();
        System.out.println(name);

        totalPrice = getPrice(elements.select("span[class=totalPrice]").text());
        regularPrice = getPrice(elements.select("span[class=regularPrice]").text());
        description = elements.select("div[class=cpt_product_description]").text();

        String imageUrl = elements.select("div[class=cpt_product_images]").select("img[id=img-current_picture]").attr("src");

        imageUrl = "http://parfumstok.ru" + imageUrl;

        image = new Image(imageUrl);
        image.load();


    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }


    public Item() {
    }

    @Override
    public String getRegularPrice() {
        return regularPrice;
    }

    @Override
    public String getTotalPrice() {
        return totalPrice;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getImage() {
        return image.toString();
    }

    private String getPrice(String text) {
        return text.replaceAll("\\D", "");
    }

    @Override
    public String toString() {
       // _CATEGORY_;_ID_;_NAME_;_MODEL_;_MANUFACTURER_;_PRICE_;_DESCRIPTION_;_IMAGE_

            StringBuilder builder = new StringBuilder("\"");

        if(categories.size() > 0) {

            for (int i = 0; i < categories.size() - 1; i++) {
                builder.append(categories.get(i)).append("\n"); //category
            }
            builder.append(categories.get(categories.size() - 1)).append("\";");
        } else {
            builder.append("\";");
            System.out.println("warning, empty category");
        }
        builder.append("0").append(";"); //id
        builder.append("\"").append(name).append("\"").append(";"); //name
        builder.append("\"").append(name).append("\"").append(";"); //model
        builder.append("\"").append(manufacture).append("\"").append(";"); //manufacture
        builder.append(totalPrice).append(";"); //price
        builder.append("\"").append(description).append("\"").append(";"); //description
        builder.append("\"").append("images/").append(image).append("\"").append("\n");

        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return name.equals(item.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
