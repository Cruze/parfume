package Model.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cruze on 02.02.2015.
 * just item list
 */
public class ItemList {
    ArrayList<iItem> items = new ArrayList<iItem>();

    public void add(List<iItem> items){
        for (iItem item : items) {
           add(item);
        }
    }

    public void add(ItemList items){
        add(items.getItems());
    }


    public void add(iItem item){
        if(!items.contains(item)){
            items.add(item);
        } else {
            int index = items.indexOf(item);
            for (String category : item.getCategory()) {
                items.get(index).addCategory(category);
            }
        }
    }

    public iItem get(int index){
        return items.get(index);
    }

    public ArrayList<iItem> getItems(){
        return items;
    }

}
