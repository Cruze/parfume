package Model.Item;

import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Cruze on 01.02.2015.
 * class for image
 */
public class Image  {
    private final String _url;
    private java.awt.Image _image = null;

    private String _name;

    public Image(String url) {
        _url = url;
    }

    public boolean load() {
        if (_image != null)
            return true;
        if (!getNameFromUrl())
            return false;
        try {
            URL url = new URL(_url);
            _image = ImageIO.read(url);
        } catch (IOException e) {
            return false;
        }
        return saveImage();
    }

    private boolean saveImage() {
        if (!checkImagesFolder())
            return false;
        try {
            File image = new File("images/" + _name);
            ImageIO.write((RenderedImage) _image, "jpg", image);
        }
        catch (Exception e) {
            return false;
        }
        return true;
    }

    private boolean checkImagesFolder() {
        try {
            File images = new File("images");
            return images.exists() || images.mkdir();
        }
        catch (Exception ignored) {
        }
        return false;
    }

    private boolean getNameFromUrl() {
        _name = _url.substring(_url.lastIndexOf("/") + 1);
//        _name = _name.substring(0, _name.indexOf("?"));
        return true;
    }

    @Override
    public String toString() {
        return _name;
    }
}
