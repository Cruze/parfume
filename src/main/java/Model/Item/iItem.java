package Model.Item;

import java.util.List;

/**
 * Created by Cruze on 01.02.2015.
 * this interface for item
 */
public interface iItem {
    void setManufacture(String manufacture);

    public void addCategory(String category);

    public List<String> getCategory();

    public void parse();

    public void setUrl(String url);

    public String getRegularPrice();

    public String getTotalPrice();

    public String getName();

    public void setName(String name);

    public String getDescription();

    public String getImage();



}
