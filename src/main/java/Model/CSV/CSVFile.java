package Model.CSV;

import Excel.ItemNames;
import Model.Item.ItemList;
import Model.Item.iItem;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Cruze on 02.02.2015.
 * this file .csv format
 */
public class CSVFile {

    private String fileName = "parfumestock.csv";

    private StringBuilder contents = new StringBuilder("_CATEGORY_;_ID_;_NAME_;_MODEL_;_MANUFACTURER_;_PRICE_;_DESCRIPTION_;_IMAGE_\n");

    public CSVFile(String fileName) {
        this.fileName = fileName;
    }

    public void add(ItemList items){
        for (iItem item : items.getItems()) {
            add(item);
        }
    }
    public void add(iItem item){
        contents.append(item.toString());
    }

    public void write(){
        File file = new File(fileName);

        try {
            FileWriter fileWriter = new FileWriter(file, false);
            fileWriter.write(contents.toString());
            fileWriter.close();
        }
        catch (Exception e) {
            System.out.println("error while writing file \"" + fileName + "\"");
        }


    }

    public ItemNames getNames() {
        ItemNames names = new ItemNames();

        File file = new File(fileName);

        BufferedReader br = null;
        try {
            br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream( file ), "UTF-8"
                    )
            );
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String line = null;
        try {
            while ((line = br.readLine()) != null) {
              String name = parseName(line);
                if (!name.equals(""))
              names.add(name);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return names;
    }

    private String parseName(String line) {
        //;0;"Agatha Ruiz de La Prada "Agua"";
        String name = "";
        Pattern p = Pattern.compile(";0;\"(.*)\";");
        Matcher matcher = p.matcher(line);
        if (matcher.find()) {
            name =  matcher.group(1).toString();
        }
        if (!name.equals(""))
            name = name.substring(0, name.indexOf(";") - 1);
        return name;
    }

}
