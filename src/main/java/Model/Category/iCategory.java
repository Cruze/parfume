package Model.Category;

/**
 * Created by Cruze on 01.02.2015.
 * interface for category
 */
public interface iCategory {

    public String getName();
    public String getUrl();


}
