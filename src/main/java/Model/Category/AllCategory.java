package Model.Category;

import Model.Category.CategoryList.AlphabetCategory;
import Model.Category.CategoryList.MaleCategory;
import Model.Category.CategoryList.PerfumeCategory;
import Model.Category.CategoryList.iCategoryList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Cruze on 01.02.2015.
 * this class contains all categories from parfumestock.ru
 */
public class AllCategory {

    private String page;

    private ArrayList<iCategoryList> categories = new ArrayList<iCategoryList>();

    public AllCategory(String page) {
        this.page = page;
        parse();
    }

    public iCategoryList getPerfumeCategories() {
        return categories.get(2);
    }

    public iCategoryList getMaleCategories() {
        return categories.get(1);
    }

    public iCategoryList getAlphabetCategories() {
        return categories.get(0);    }

    public void parse(){

        iCategoryList perfumeCategories  = new PerfumeCategory();
        iCategoryList maleCategories     = new MaleCategory();
        iCategoryList alphabetCategories = new AlphabetCategory();

        org.jsoup.nodes.Document doc = Jsoup.parse(page);
        Elements cssmenu = doc.select("ul[id=cssmenu]").select("ul");

        cssmenu.remove(0);

        for (Element li : cssmenu.get(0).select("li")) {
            perfumeCategories.add(new Category(li.text(), li.select("a").attr("href")));
        }
        cssmenu.remove(0);

        for (Element li : cssmenu.get(0).select("li")) {
            maleCategories.add(new Category(li.text(), li.select("a").attr("href")));
        }
        cssmenu.remove(0);


        for (Element li : cssmenu.select("li")) {
            alphabetCategories.add(new Category(li.text(), li.select("a").attr("href")));
        }
        categories.add(alphabetCategories);
        categories.add(maleCategories);
        categories.add(perfumeCategories);
    }

    public ArrayList<iCategoryList> getCategories() {
        return categories;
    }
}
