package Model.Category;

/**
 * Created by Cruze on 01.02.2015.
 * this class for one category. for example "adidas"
 */
public class Category implements iCategory {

    private String name;
    private String url;

    public Category(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "url='" + url + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
