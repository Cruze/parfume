package Model.Category.CategoryList;

import Model.Category.iCategory;
import Model.Item.ItemList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cruze on 02.02.2015.
 * absctract class is cool
 */
abstract class AbstractCategoryList implements iCategoryList {
    List<iCategory> categories = new ArrayList<iCategory>();

    @Override
    public void add(iCategory category) {
        categories.add(category);
    }

    @Override
    public abstract ItemList getItems();

    @Override
    public iCategory get(int i) {
        return categories.get(i);
    }

    @Override
    public int size() {
        return categories.size();
    }
}
