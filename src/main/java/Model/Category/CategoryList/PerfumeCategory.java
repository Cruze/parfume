package Model.Category.CategoryList;

import Model.Category.iCategory;
import Model.Item.Item;
import Model.Item.ItemList;
import Model.Item.iItem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import static Model.Utils.Utils.PARFUMESTOK;
import static Model.Utils.Utils.getPage;

/**
 * Created by Cruze on 01.02.2015.
 * perfume categories
 */
public class PerfumeCategory extends AbstractCategoryList{

    @Override
    public ItemList getItems() {
        ItemList itemList = new ItemList();

       for (iCategory category : categories) {
            itemList.add(parse(category));
        }
        return itemList;
    }

    private ItemList parse(iCategory category) {
        ItemList itemList = new ItemList();
        String url = category.getUrl();
        String page = getPage(url);
        org.jsoup.nodes.Document doc = Jsoup.parse(page);
        Elements elements = doc.select("a.no_underline");

        if(elements.size() > 1 ) {
            Element element = elements.get(elements.size() - 2);
            if (element.text().equals("след >>")){
                element = elements.get(elements.size() - 3);
            }
            int countPage = Integer.parseInt(element.text());
            url = PARFUMESTOK + elements.get(0).attr("href");
            url = url.replaceAll("30", "");
            for (int i = 0; i < countPage - 1; i++) {
                itemList.add(getItemListFromPage(page, category.getName()));
                int count = (i + 1) * 30;
                page = getPage(url + count);
            }
        } else {
            itemList.add(getItemListFromPage(page, category.getName()));
        }
        for (iItem iItem : itemList.getItems()) {
            System.out.println(iItem.getName());
        }
        return itemList;
    }

    private ItemList getItemListFromPage(String page, String category) {
        ItemList itemList = new ItemList();
        org.jsoup.nodes.Document doc = Jsoup.parse(page);
        Elements elements = doc.select("center").select("table[cellpadding]").select("td[valign=top]");

        String name;
        String url;

        for (Element element : elements) {

            name = element.select("div[class=prdbrief_name]").text();
            url = element.select("div[class=prdbrief_name]").select("a").attr("href");
            url = PARFUMESTOK + url;
            iItem item = new Item();
            item.setName(name);
            item.setUrl(url);
            System.out.println(name);
            item.addCategory("Парфюмерия|" + category);
            itemList.add(item);
        }
        return itemList;
    }

}
