package Model.Category.CategoryList;

import Model.Category.iCategory;
import Model.Item.ItemList;

/**
 * Created by Cruze on 01.02.2015.
 * interface for all categories
 */
public interface iCategoryList {

    public void add(iCategory category);

    public ItemList getItems();

    public iCategory get(int i);

    public int size();

}
