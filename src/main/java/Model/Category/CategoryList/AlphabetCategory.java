package Model.Category.CategoryList;

import Model.Category.iCategory;
import Model.Item.Item;
import Model.Item.ItemList;
import Model.Item.iItem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import static Model.Utils.Utils.PARFUMESTOK;
import static Model.Utils.Utils.getPage;

/**
 * Created by Cruze on 01.02.2015.
 * alphabet categories
 */
public class AlphabetCategory extends AbstractCategoryList{



    @Override
    public ItemList getItems() {
        ItemList itemList = new ItemList();
        for (iCategory category : categories) {
            String url = category.getUrl();

            String page = getPage(url);
            org.jsoup.nodes.Document doc = Jsoup.parse(page);
            Elements elements = doc.select("a.no_underline");

            int countPage = getCountPage(doc);

            if(countPage > 1 ) {
                url = PARFUMESTOK + elements.get(0).attr("href");
                url = url.replaceAll("30", "");
                for (int i = 0; i < countPage - 1; i++) {
                    itemList.add(getItemListFromPage(category, doc));
                    int count = (i + 1) * 30;
                    page = getPage(url + count);
                    doc = Jsoup.parse(page);
                }
            } else {
                itemList.add(getItemListFromPage(category, doc));
            }

        }
        return itemList;
    }

    private ItemList getItemListFromPage(iCategory category, Document doc) {
        ItemList itemList = new ItemList();
        Elements elements;
        elements = doc.select("center").select("table[cellpadding]").select("td[valign=top]");
        for (Element item : elements) {
            itemList.add(getItem(item, category.getName()));
        }
        return itemList;
    }


    public int getCountPage(Document doc) {

        Elements elements = doc.select("div.main-content").select("center");
        if (!elements.text().contains("след >>")){
            return 1;
        } else {
            if(elements.text().contains("показать все")){
                elements = elements.select("a.no_underline").select("a");
                return Integer.parseInt(elements.get(0).text());
            }else{
                elements = elements.select("a.no_underline").select("a");
                return Integer.parseInt(elements.get(elements.size() - 2).text());
            }
        }
    }



    private iItem getItem(Element element, String manufacture) {
        String url = element.select("div[class=prdbrief_name]").select("a").attr("href");
        url = PARFUMESTOK + url;
        String page = getPage(url);

        iItem item = new Item(page);
        item.setManufacture(manufacture);

        item.parse();
        return item;
    }
}
