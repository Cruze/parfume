package Model.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

/**
 * Created by Cruze on 01.02.2015.
 * getPage method class
 */
public class Utils {

    public static String PARFUMESTOK = "http://parfumstok.ru";

    public static String getPage(String url){
        HttpClient httpClient = HttpClientBuilder.create().build() ;
        HttpGet httpGet = new HttpGet(url);
        HttpResponse response;
        String page = "";
        try {
            response = httpClient.execute(httpGet);
            page = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return page;
    }

}
