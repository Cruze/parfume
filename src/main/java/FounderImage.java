import Model.Item.Image;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static Model.Utils.Utils.getPage;

/**
 * Created by Cruze on 13.02.2015.
 */
public class FounderImage {

    private static String url = "E:\\work\\parfume\\image.xls";
    private static String found = "http://www.parfumstok.ru/index.php?ukey=search&searchstring=";
    private static String csvUrl = "E:\\work\\parfume\\perfume.csv";


    public static void main(String[] args) {
        List<String> names = new ArrayList<String>();

        names = getNames();

       ArrayList<String> csv = getCsv();

        int k = 0;
        for (String name : names) {

            String urlForFound = found + name.replaceAll(" ", "+");
            String page = getPage(urlForFound);

            org.jsoup.nodes.Document doc = Jsoup.parse(page);
            Elements elements = doc.select("center").select("table[cellpadding]").select("td[valign=top]");
            for (Element element : elements) {
                String nameElement = element.select("div[class=prdbrief_name]").text().replaceAll("\"", "");
                if (nameElement.equals(name)) {
                    String urlImage = element.select("img").attr("src");
                    if (!urlImage.equals("")) {
                        Image image = new Image("http://parfumstok.ru/" + urlImage);
                        image.load();
                        urlImage = "images/" + image.toString();
                        csv = writeImage(csv, name, urlImage);
                    }
                    System.out.println(k++ + ": " + name + "href: " + urlImage);
                }
            }

            writeCsv(csv);

        }

    }

    private static void writeCsv(ArrayList<String> csv) {
        String fileName = "newPerfume.csv";
        File file = new File(fileName);

        try {
            FileWriter fileWriter = new FileWriter(file, false);
            for (String s : csv) {
                fileWriter.write(s + "\n");
            }

            fileWriter.close();
        }
        catch (Exception e) {
            System.out.println("error while writing file \"" + fileName + "\"");
        }
    }

    private static ArrayList<String> writeImage(ArrayList<String> csv, String name, String urlImage) {
        ArrayList<String> strings = new ArrayList<String>();

        for (String s : csv) {
            if (s.replaceAll("\"", "").contains(name)){
                System.out.println("hello");
               s = s.replaceAll("images/parfumstok.ru", urlImage);
            }
            strings.add(s);
        }

        return strings;
    }

    private static ArrayList<String> getCsv() {
        File file = new File(csvUrl);

        BufferedReader br = null;

        br = getBufferedReader(file);

        ArrayList<String> strings = new ArrayList<String>();

        String line = null;

        try {
            while ((line = br.readLine()) != null) {
                strings.add(line);
                }
            } catch (IOException e1) {
            e1.printStackTrace();
        }


        close(br);
        return strings;
    }

    private static void close(BufferedReader br)  {
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static BufferedReader getBufferedReader(File file)  {
        BufferedReader br = null;
        try {
            br = new BufferedReader(
                    new InputStreamReader( new FileInputStream( file ), "UTF-8")
                );
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return br;
    }


    private static String getImageUrl(String page, String name) {
        String url = "";

        org.jsoup.nodes.Document doc = Jsoup.parse(page);
        Elements elements = doc.select("div[class=main-content]");


        return url;
    }

    private static List<String> getNames() {
        List<String> names = new ArrayList<String>();
        HSSFWorkbook wb = getHSSFWorkbook();
        Sheet sheet = wb.getSheetAt(0);
        for (Row row : sheet) {
           Cell cell = row.getCell(3);
            names.add(cell.getStringCellValue().replaceAll("\"", ""));
        }
            return names;
    }

    private static HSSFWorkbook getHSSFWorkbook() {
        try {
            return new HSSFWorkbook(getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("i can't initialize new HSSFWorkbook");
            return null;
        }
    }

    private static InputStream getInputStream() {
        try {
            return new FileInputStream(url);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("I can't find this file: " + url);
            return null;
        }
    }
}
