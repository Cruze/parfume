import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Cruze on 14.02.2015.
 */
public class FUCK {

    static String url = "E:\\work\\parfume\\Found.xls";

    public static void main(String[] args) throws IOException {

        ArrayList<String> csv = new ArrayList<String>();
        ArrayList<String> excel = new ArrayList<String>();

        HSSFWorkbook wb = getHSSFWorkbook();
        Sheet sheet = wb.getSheetAt(0);
        for (Row row : sheet) {
            if (row.getCell(0) != null) {
                excel.add(row.getCell(0).getStringCellValue());
                csv.add(row.getCell(1).getStringCellValue());
            }
        }

        int k = 0;
        Map<String, String> pair = new TreeMap<String, String>();
        for (int i = 0; i < csv.size(); i++) {
            String name = buildNewName(csv.get(i), excel.get(i));
            pair.put(csv.get(i), name);
            System.out.println(k++ + ": " + csv.get(i) + " + " + excel.get(i) + " = " + name);
        }



        File fileExcel = new File("temp.xls");

        wb = new HSSFWorkbook();
        Sheet sh = wb.createSheet();
        int rowCount = 0;
        for (Map.Entry<String, String> entry : pair.entrySet()) {

            Row row = sh.createRow(rowCount++);
            Cell cellCsv = row.createCell(0);
            Cell cellNew = row.createCell(1);
            cellCsv.setCellValue(entry.getKey());
            cellNew.setCellValue(entry.getValue());
            FileOutputStream outer =
                    new FileOutputStream(fileExcel);
            wb.write(outer);
            outer.close();


        }



    }


    private static String buildNewName(String nameCsv, String nameExcel) {
        //Gucci "Guilty Glossy"woman edT 75ml = Gucci"Guilty Glossy"" woman edT 75ml NEW
        nameCsv = replace(nameCsv);
        if (!nameCsv.contains(getType(nameExcel))){
            nameCsv = nameCsv + " " + getType(nameExcel);
        }
        nameCsv = nameCsv + " " + getMl(nameExcel);
        nameCsv = nameCsv + getTester(nameExcel);

        return nameCsv;
    }

    private static String getTester(String nameExcel) {
        if (nameExcel.toLowerCase().contains("тестер")){
            return ", тестер";
        }
        return "";
    }

    private static String getMl(String nameExcel) {
        Pattern pattern = Pattern.compile("(\\d+\\,\\d+)ml");
        Matcher matcher = pattern.matcher(nameExcel);
        if (matcher.find()) {
            return matcher.group(1) + " мл";
        } else {
            pattern = Pattern.compile("(\\d+\\.\\d+)ml");
            matcher = pattern.matcher(nameExcel);
            if (matcher.find()) {
                return matcher.group(1) + " мл";
            } else {
                pattern = Pattern.compile("(\\d+)ml");
                matcher = pattern.matcher(nameExcel);
                if (matcher.find()) {
                    return matcher.group(1) + " мл";
                } else {
                    pattern = Pattern.compile("(\\d+) мл");
                    matcher = pattern.matcher(nameExcel);
                    if (matcher.find()) {
                        return matcher.group(1) + " мл";
                    } else {
                        pattern = Pattern.compile("(\\d+) ml");
                        matcher = pattern.matcher(nameExcel);
                        if (matcher.find()) {
                            return matcher.group(1) + " мл";
                        }
                        pattern = Pattern.compile("(\\d+) ml");
                        matcher = pattern.matcher(nameExcel);
                        if (matcher.find()) {
                            return matcher.group(1) + " мл";
                        } else {
                            pattern = Pattern.compile("(\\d+)g");
                            matcher = pattern.matcher(nameExcel);
                            if (matcher.find()) {
                                return matcher.group(1) + " гр";
                            }
                        }
                    }
                }
            }
        }
        return "";
    }

    private static String getType(String nameExcel) {
        if (nameExcel.contains("edT")){
            return "туалетная вода";
        } else if (nameExcel.contains("edP")){
            return "парфюмерная вода";
        } else if (nameExcel.toLowerCase().contains("сухие духи")){
            return "сухие духи";
        } else if (nameExcel.toLowerCase().contains("духи")){
            return "духи";
        } else {
            return "";
        }
    }

    private static String replace(String nameCsv) {
        nameCsv =  nameCsv.replaceAll("edP", "");
        nameCsv = nameCsv.replaceAll("edT", "");
        nameCsv = nameCsv.replaceAll("(\\d+)ml", "").trim();
        //TODO replace ml number
        return nameCsv;
    }


    private static HSSFWorkbook getHSSFWorkbook() {
        try {
            return new HSSFWorkbook(getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("i can't initialize new HSSFWorkbook");
            return null;
        }
    }

    private static InputStream getInputStream() {
        try {
            return new FileInputStream(url);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("I can't find this file: " + url);
            return null;
        }
    }


}
